<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Waste extends Model
{
    protected $fillable = ['name'];

    public function containers(){
        return $this->belongsToMany('App\Models\Container');
    }
}
