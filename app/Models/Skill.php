<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['category', 'description'];

    public function advice(){
        return $this->hasMany('App\Models\Advice');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User');
    }
}
