<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    protected $fillable = ['type', 'subtype'];

    public function wastes(){
        return $this->belongsToMany('App\Models\Waste');
    }
}
