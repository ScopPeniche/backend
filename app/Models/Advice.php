<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advice extends Model
{
    protected $fillable = ['category', 'description'];

    public function skill(){
        return $this->belongsTo('App\Models\Skill');
    }
}
