<?php namespace App\Http\Repositories\V1\UserAccount;

use App\Http\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\Trash;
use App\Models\User;

class TrashRepository extends BaseRepository
{
	public function __construct(Trash $trash)
	{
		$this->model = $trash;
	}
	public function index()
	{
		return $this->model->get();
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $trash = new $this->model;
	    $trash->fill($inputs);
	    $trash->save();
	}

	public function update($inputs, $id)
	{		
		$trash = $this->model->find($id);
		$trash->fill($inputs);
		$trash->save();
	}
	public function destroy($id)
	{
		$trash = $this->model->find($id);
		$trash->delete();
	}  

	public function sync($inputs)
	{
		$user = User::find(Auth::id());
		$trashes = array_merge($inputs['grise'],$inputs['tri']);
		$user->trashes()->sync($trashes);
	}
}