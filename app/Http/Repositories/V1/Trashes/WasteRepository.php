<?php namespace App\Http\Repositories\V1\Trashes;

use App\Http\Repositories\BaseRepository;
use App\Models\Waste;

class WasteRepository extends BaseRepository
{
	public function __construct(Waste $waste)
	{
		$this->model = $waste;
	}
	public function index()
	{
		return $this->model->get();
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $waste = new $this->model;
	    $waste->fill($inputs);
	    $waste->save();
	}

	public function update($inputs, $id)
	{		
		$waste = $this->model->find($id);
		$waste->fill($inputs);
		$waste->save();
	}
	public function destroy($id)
	{
		$waste = $this->model->find($id);
		$waste->delete();
	}  
}