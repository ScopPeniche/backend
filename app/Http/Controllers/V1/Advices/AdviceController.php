<?php 
namespace App\Http\Controllers\V1\Advices;

use Illuminate\Routing\Controller;
use App\Http\Repositories\V1\Advices\AdviceRepository;
use App\Http\Resources\V1\Advices\AdviceResource;
use Response;

class AdviceController extends Controller {
	
	protected $advices;

    public function __construct(AdviceRepository $advices)
    {
        $this->advices = $advices;
    }

    public function index()
    {
        return AdviceResource::collection($this->advices->index());
    }
}