<?php 
namespace App\Http\Controllers\V1\UserAccount;

use App\Models\Home;
use Illuminate\Routing\Controller;
use App\Http\Repositories\V1\UserAccount\HomeRepository;
use App\Http\Resources\V1\UserAccount\HomeResource;
use App\Http\Requests\Homes\CreateRequest;
use App\Http\Requests\Homes\UpdateRequest;
use Response;

class HomeController extends Controller {
	
	protected $homes;

    public function __construct(HomeRepository $homes)
    {
        $this->homes = $homes;
    }

    public function index()
    {
        return HomeResource::collection($this->homes->index());
    }

    public function show($id)
    {
        return HomeResource::collection($this->homes->find($id));
    }

    public function store(CreateRequest $request)
    {
        $this->homes->store($request->all());
        return response()->json(['status'=>'success','msg'=>'Foyer créé']);
    }

    public function update(UpdateRequest $request, $id){
        $this->homes->update($request->all(), $id);
        return response()->json(['status'=>'success','msg'=>'Foyer modifié']);
    }
    
    public function destroy($id){
        $this->homes->destroy($id);
        return response()->json(['status'=>'success','msg'=>'Foyer supprimé']);
    }
}