<?php 
namespace App\Http\Controllers\V1\UserAccount;

use Illuminate\Routing\Controller;
use App\Http\Repositories\V1\UserAccount\TrashRepository;
use App\Http\Resources\V1\UserAccount\TrashResource;
use App\Http\Requests\Trashes\CreateRequest;
use App\Http\Requests\Trashes\UpdateRequest;
use Illuminate\Http\Request;
use Response;

class TrashController extends Controller {
	
	protected $trashes;

    public function __construct(TrashRepository $trashes)
    {
        $this->trashes = $trashes;
    }

    public function index()
    {
        return TrashResource::collection($this->trashes->index());
    }

    public function show($id)
    {
        return TrashResource::collection($this->trashes->find($id));
    }

    public function store(CreateRequest $request)
    {
        $this->trashes->store($request->all());
        return response()->json(['status'=>'success','msg'=>'Poubelle créée']);
    }

    public function update(UpdateRequest $request, $id){
        $this->trashes->update($request->all(), $id);
        return response()->json(['status'=>'success','msg'=>'Poubelle modifiée']);
    }
    
    public function destroy($id){
        $this->trashes->destroy($id);
        return response()->json(['status'=>'success','msg'=>'Poubelle supprimée']);
    }

    public function sync(Request $request){
        $this->trashes->sync($request->all());
        return response()->json(['status'=>'success','msg'=>'Poubelle(s) associée(s)']);
    }
}