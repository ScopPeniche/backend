<?php 
namespace App\Http\Controllers\V1\UserAccount;

use App\Models\Dump;
use Illuminate\Routing\Controller;
use App\Http\Repositories\V1\UserAccount\DumpRepository;
use App\Http\Resources\V1\UserAccount\DumpResource;
use App\Http\Requests\Dumps\CreateRequest;
use Response;

class DumpController extends Controller {
	
	protected $dumps;

    public function __construct(DumpRepository $dumps){
        $this->dumps = $dumps;
    }

    public function index(){
        return DumpResource::collection($this->dumps->index());
    }

    public function show($id){
        return DumpResource::collection($this->dumps->find($id));
    }

    public function store(CreateRequest $request){
        $this->dumps->store($request->all());
        return response()->json(['status'=>'success','msg'=>'Poubelle jetée']);
    }

    public function destroy($id){
        $this->dumps->destroy($id);
        return response()->json(['status'=>'success','msg'=>'Jeté de poubelle supprimé']);
    }
}