<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user && $user->isAdmin())
        {
            return $next($request);
        }
        return response()->json(['status'=>'error','msg'=>'Vous n\'êtes pas autorisé à accéder à cette ressource'],401);
    }
}