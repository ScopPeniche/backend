<?php

namespace App\Http\Resources\V1\Advice;

use Illuminate\Http\Resources\Json\JsonResource;

class AdviceResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
        ];
    }
}