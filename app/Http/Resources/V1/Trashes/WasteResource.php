<?php

namespace App\Http\Resources\V1\Trashes;

use Illuminate\Http\Resources\Json\JsonResource;

class WasteResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'containers' => ContainerResource::collection($this->containers)
        ];
    }
}