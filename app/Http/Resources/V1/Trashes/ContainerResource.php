<?php

namespace App\Http\Resources\V1\Trashes;

use Illuminate\Http\Resources\Json\JsonResource;

class ContainerResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'subtype' => $this->subtype
        ];
    }
}