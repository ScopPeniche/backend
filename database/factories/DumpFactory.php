<?php

use Faker\Generator as Faker;
use App\Models\Dump;
$factory->define(Dump::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'trash_id' => $faker->numberBetween(1,2),
        'full' => $faker->boolean,
        'date' => $faker->date($format = 'Y-m-d'),
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now()
    ];
});
