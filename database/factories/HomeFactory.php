<?php

use App\Models\Home;
use Faker\Generator as Faker;

$factory->define(Home::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(['appartement','maison']),
        'residents' => $faker->numberBetween(1,4),
        'gender' => $faker->randomElement(['garçon', 'fille','mixte']),
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now()
    ];
});
