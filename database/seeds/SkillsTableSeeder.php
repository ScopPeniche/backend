<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SkillsTableSeeder extends Seeder
{

    public function run()
    {
        Model::unguard();
        DB::table('skills')->delete();
		DB::statement("
            insert into ptitepoubelle.skills(id,category,description,points,created_at,updated_at) 
            select id,category,description,points,now(),now()
            from sources.skills
            order by id;"
        );
        Model::reguard();
    }
}