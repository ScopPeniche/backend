<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContainersTableSeeder extends Seeder
{

    public function run()
    {
        Model::unguard();
        DB::table('containers')->delete();
		DB::statement("
            insert into ptitepoubelle.containers(id,type,subtype,created_at,updated_at) 
            select id,type,subtype,now(),now()
            from sources.containers
            order by id;"
        );
        Model::reguard();
    }
}
