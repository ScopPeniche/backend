<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WastesTableSeeder extends Seeder
{

    public function run()
    {
        Model::unguard();
        DB::table('wastes')->delete();
		DB::statement("
            insert into ptitepoubelle.wastes(id,name,created_at,updated_at) 
            select id,name,now(),now()
            from sources.wastes
            order by id;"
        );
        Model::reguard();
    }
}
