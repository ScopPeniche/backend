<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CollectPointsTableSeeder extends Seeder
{

    public function run()
    {
        Model::unguard();
        DB::table('collect_points')->delete();
		DB::statement("
            insert into ptitepoubelle.collect_points(address,city,volume,waste_type,container_type,lon_x,lat_y,created_at,updated_at) 
            select address,city,volume,waste_type,container_type,lon_x,lat_y,now(),now()
            from sources.collect_points;"
        );
        Model::reguard();
    }
}