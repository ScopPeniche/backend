<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContainerWasteTableSeeder extends Seeder
{
    
    public function run()
    {
        Model::unguard();
        DB::table('container_waste')->delete();
		DB::statement("
            insert into ptitepoubelle.container_waste(id,container_id,waste_id) 
            select *
            from sources.container_waste
            order by id;"
        );
        Model::reguard();
    }
}
